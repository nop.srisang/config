package config

import (
	"errors"
	yaml "gopkg.in/yaml.v2"
	"os"
	"strconv"
)

// Configuration type
type Configuration struct {
	Port    int     `yaml:"port"`
	Jwt     Jwt     `yaml:"jwt"`
	MongoDB MongoDB `yaml:"mongodb,omitempty"`
	RedisDB RedisDB `yaml:"redis,omitempty"`
	Socket  Socket  `yaml:"socket,omitempty"`
	Env     Env     `yaml:"-"`
}

// Jwt config type
type Jwt struct {
	Private string `yaml:"private"`
	Public  string `yaml:"public"`
}

// MongoDB config type
type MongoDB struct {
	Host     string `yaml:"host"`
	Database string `yaml:"database"`
	Username string `yaml:"username,omitempty"`
	Password string `yaml:"password,omitempty"`
}

// RedisDB type
type RedisDB struct {
	Addr     string `yaml:"addr"`
	Password string `yaml:"password"`
	DB       int    `yaml:"db"`
}

// Socket config type
type Socket struct {
	URL string `yaml:"url"`
}

// Env type
type Env struct {
	VarName string
	EnvName string
}

var (
	// ErrConfigFileNotFound error can not open configuration file
	ErrConfigFileNotFound = errors.New("can not open configuration file")
	// ErrParseConfigFile error can not parse configuration file
	ErrParseConfigFile = errors.New("can not parse configuration file")
)

// GetConfig return configuration
func GetConfig(envVariable string) (*Configuration, error) {
	var (
		configuration Configuration
		envName       = ""
	)
	if os.Getenv(envVariable) != "" {
		configuration.Env.VarName = envVariable
		configuration.Env.EnvName = os.Getenv(envVariable)
		envName = configuration.Env.EnvName + "."
	}
	file, err := os.Open("./config." + envName + "yaml")
	if err != nil {
		return nil, ErrConfigFileNotFound
	}
	decoder := yaml.NewDecoder(file)

	err = decoder.Decode(&configuration)
	if err != nil {
		return nil, ErrParseConfigFile
	}
	if port := os.Getenv("PORT"); port != "" {
		if portNo, err := strconv.Atoi(port); err == nil {
			configuration.Port = portNo
		}
	}
	return &configuration, err
}

// IsProd return true when environment is production
func (c *Configuration) IsProd() bool {
	return c.Env.EnvName == "production"
}

// GetEnvName return environment name
func (c *Configuration) GetEnvName() string {
	return c.Env.EnvName
}

// GetEnvVarName return environment variable name
func (c *Configuration) GetEnvVarName() string {
	return c.Env.VarName
}
