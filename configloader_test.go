package config

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

const (
	envProd    = "production"
	envTest    = "test"
	envVarName = "TEST_VAR"
)

func TestConfigValues(t *testing.T) {
	conf, err := GetConfig("")
	if err == ErrConfigFileNotFound {
		t.Fatal("config.test.yaml is not found")
	}
	if err == ErrParseConfigFile {
		t.Fatal("config.test.yaml can not be parsed")
	}
	if envName := conf.GetEnvName(); envName != conf.Env.EnvName || envName != "" {
		t.Error("environment name is not match")
	}
	assert.Equal(t, &Configuration{
		Port: 1330,
		Jwt: Jwt{
			Private: "privatekey",
			Public:  "publickey",
		},
		MongoDB: MongoDB{
			Host:     "localhost",
			Database: "dbname",
			Username: "username",
			Password: "password!",
		},
		Socket: Socket{
			URL: "socketurl",
		},
	}, conf, "Must be match config object")
}
func TestGetValueFromVar(t *testing.T) {
	// Declare environment variable and ser value

	os.Setenv(envVarName, envTest)

	conf, err := GetConfig(envVarName)

	if err == ErrConfigFileNotFound {
		t.Fatal("config.test.yaml is not found")
	}
	if err == ErrParseConfigFile {
		t.Fatal("config.test.yaml can not be parsed")
	}
	if envName := conf.GetEnvName(); envName != conf.Env.EnvName || envName != envTest {
		t.Error("environment name is not match")
	}
	if conf.GetEnvVarName() != envVarName {
		t.Error("environment variable name is not match")
	}
}

func TestEmptyVarName(t *testing.T) {
	conf, err := GetConfig("")

	if err == ErrConfigFileNotFound {
		t.Fatal("config.yaml is not found")
	}
	if envName := conf.GetEnvName(); envName != conf.Env.EnvName || envName != "" {
		t.Error("environment name is not match")
	}
}

func TestProductionEnv(t *testing.T) {
	os.Setenv(envVarName, envProd)

	conf, err := GetConfig(envVarName)

	if err == ErrConfigFileNotFound {
		t.Fatal("config.test.yaml is not found")
	}
	if err == ErrParseConfigFile {
		t.Fatal("config.test.yaml can not be parsed")
	}
	if envName := conf.GetEnvName(); envName != conf.Env.EnvName || envName != envProd {
		t.Error("environment name is not production")
	}
	if !conf.IsProd() {
		t.Error("environment is not production")
	}
}
